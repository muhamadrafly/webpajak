


        <div class="container-fluid mt-5 pt-5">

          <div class="text-center">
            <div class="error mx-auto" data-text="404">404</div>
            <p class="lead text-gray-800 mb-5">Laman tidak ditemukan!</p>
            <p class="text-gray-500 mb-0">Sepertinya kamu menemukan kesalahan dalam matrix...</p>
            <a href="<?= base_url('layout'); ?>">&larr; <u>Kembali ke Menu?</u></a>
          </div>

        </div>
