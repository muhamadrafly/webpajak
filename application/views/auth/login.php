
  <div class="container">

    <!-- Outer Row -->
    <div class="row mt-5 pt-4">

      <div class="float-left ml-5 pt-5 mt-5 mr-5">
        <img src="<?= base_url('assets'); ?>/img/3.svg" width="500">
      </div>

      <div class="col-xl-5 col-lg-6 col-md-5">

        <div class="my-1">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <h1 class="text-center"><img src="<?= base_url('assets'); ?>/img/logo.png" width="50"> BPPD</h1>

                <div class="p-5">

                  <div class="text-center">

                    <h1 class="h4 mb-4">Login Form</h1>
                  </div>
                  <?= $this->session->flashdata('message'); ?>
                  <form class="user" method="post" action="<?= base_url('auth'); ?>">
                    <div class="form-group">
                      <input type="text" name="email" class="form-control form-control-user" id="email" aria-describedby="emailHelp" placeholder="Email Address..." autofocus>
                        <?= form_error('email', '<small class="text-danger float-left mb-3 mt-1 ml-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control form-control-user" id="password" placeholder="Password" >
                        <?= form_error('password', '<small class="text-danger float-left mb-3 mt-1 ml-3">', '</small>'); ?>
                    </div>

                    <button class="btn btn-primary btn-user btn-block">
                      Login
                    </button>

                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <h6> <strong>&copy;</strong> Copyright <strong><script>document.write(new Date().getFullYear());</script></strong> Badan Pengelola Pendapatan Daerah | Kabupaten Cianjur</h6>
              <a class="btn btn-default submit" href="<?= base_url('layout'); ?>"><i class="fa fa-arrow-left"></i> <u>Kembali ke Menu</u></a>
            </div>
          </div>
        </footer>
      </div>



    </div>

  </div>
