

<div class="container-fluid">
  <div class="col-lg-5">
    <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
      <i class="fas fa-plus"></i> Form Tambah Data
    </div>

    <form action="<?= base_url('administrator/bidang/input_aksi'); ?>" method="post">

      <div class="form-group">
        <label for="kode_bidang">Kode Bidang</label>
        <input type="text" id="kode_bidang" name="kode_bidang" class="form-control" placeholder="Masukan Kode Bidang" autofocus>
        <?= form_error('kode_bidang', '<div class="text-danger small" ml-3 pt-1>'); ?>
      </div>
      <div class="form-group">
        <label for="bidang">Nama Bidang</label>
        <input type="text" id="bidang" name="bidang" class="form-control" placeholder="Masukan Nama Bidang">
          <?= form_error('bidang', '<div class="text-danger small" ml-3>'); ?>
      </div>

      <button type="submit" name="button" class="btn btn-primary">Simpan</button>
      <a class="btn btn-secondary" href="<?= base_url('administrator/bidang'); ?>">Kembali</a>

    </form>
  </div>
</div>
