<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-user-tie"></i> Daftar Pejabat
  </div>

  <?= $this->session->flashdata('message'); ?>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>Nama</th>
      <th>NIP</th>
      <th>Jabatan</th>
      <th>Aksi</th>
    </tr>

    <?php $i = 1; ?>
    <?php foreach ($pejabat as $pj) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $pj->nama; ?></td>
        <td><?= $pj->nip; ?></td>
        <td><?= $pj->jabatan; ?></td>
        <td width="20px"><?= anchor('administrator/pejabat/update/'. $pj->id,'<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>' ); ?></td>
      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>
