<div class="container-fluid">
  <div class="col-lg-5">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-edit"></i> Form Update
  </div>

  <?php foreach ($bagian as $bg) : ?>

    <form class="" action="<?= base_url('administrator/bagian/update_aksi'); ?>" method="post">
      <div class="form-group">
        <label for="kode_bagian">Kode Bagian</label>
        <input type="hidden" name="id_bagian" value="<?= $bg->id_bagian ?>">
        <input type="text" name="kode_bagian" class="form-control" id="kode_bagian" value="<?= $bg->kode_bagian ?>" autofocus>
      </div>
      <div class="form-group">
        <label for="bagian">Nama Bagian</label>
        <input type="text" name="bagian" class="form-control" id="bagian" value="<?= $bg->bagian ?>">
      </div>
      <div class="form-group">
        <label for="bidang">Bidang</label>
        <select class="form-control" name="bidang" id="bidang">
          <option value="<?= $bg->bidang ?>">-- <?= $bg->bidang ?> --</option>

          <?php foreach ($bidang as $bd) : ?>
          <option value="<?= $bd->bidang ?>"><?= $bd->bidang ?></option>
        <?php endforeach; ?>
        </select>
      </div>

      <button type="submit" name="button" class="btn btn-primary">Update</button>
      <a class="btn btn-secondary" href="<?= base_url('administrator/bagian'); ?>">Kembali</a>
    </form>
  <?php endforeach; ?>
  </div>
</div>
