      <div class="container-fluid">
        <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
          <i class="fas fa-home"></i> Dashboard
        </div>

        <div class="alert alert-info" role="alert">
          <h5 class="alert-heading">Selamat datang <?= $user['name']; ?></h5>
          <p>Aww yeah, Selamat datang di System Admin Badan Pengelola Pendapatan Daerah ,
            <?php if ($user['level'] == 'admin') : ?>
              Anda login sebagi Admin resmi
            <?php endif; ?></p>
          <hr>
          <p class="mb-0"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              <i class="fas fa-cogs"></i> Control Panel
            </button></p>
        </div>
      </div>
      </div>


      <!-- Button trigger modal -->


      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog col-lg-3" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-cogs"></i> Control Panel</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-user-tie "></i>
                  <a href="<?= base_url('administrator/pejabat'); ?>">
                    <p class="nav-link
                   text-info">Pejabat</p>
                  </a>
                </div>
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-users"></i>
                  <a href="<?= base_url('administrator/karyawan'); ?>">
                    <p class="nav-link
                   text-info">Karyawan</p>
                  </a>
                </div>
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-city"></i>
                  <a href="<?= base_url('administrator/bidang'); ?>">
                    <p class="nav-link
                   text-info">Bidang</p>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-building "></i>
                  <a href="<?= base_url('administrator/bagian'); ?>">
                    <p class="nav-link
                   text-info">Bagian</p>
                  </a>
                </div>
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-users-cog"></i>
                  <a href="<?= base_url('administrator/users'); ?>">
                    <p class="nav-link
                  text-info">User Control</p>
                  </a>
                </div>
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-folder-open"></i>
                  <a href="<?= base_url('administrator/menu'); ?>">
                    <p class="nav-link
                    text-info">Menu User</p>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-file-alt"></i>
                  <a href="<?= base_url('administrator/layanan'); ?>">
                    <p class="nav-link
                  text-info">Layanan</p>
                  </a>
                </div>
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-id-card-alt "></i>
                  <a href="<?= base_url('administrator/identitas') ?>">
                    <p class="nav-link
                  text-info">Indentitas</p>
                  </a>
                </div>
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-info-circle"></i>
                  <a href="<?= base_url('administrator/tentang'); ?>">
                    <p class="nav-link
                    text-info">Tentang</p>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-bullhorn"></i>
                  <a href="<?= base_url('administrator/info'); ?>">
                    <p class="nav-link
                  text-info">Info</p>
                  </a>
                </div>
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-newspaper"></i>
                  <a href="<?= base_url('administrator/news'); ?>">
                    <p class="nav-link
                  text-info">News</p>
                  </a>
                </div>
                <div class="col-md-4 text-info text-center">
                  <i class="fas fa-3x fa-envelope-open-text"></i>
                  <a href="<?= base_url('administrator/kontak'); ?>">
                    <p class="nav-link
                  text-info">Message</p>
                  </a>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-info" data-dismiss="modal">Close Panel</button>
            </div>
          </div>
        </div>
      </div>