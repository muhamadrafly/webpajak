
<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-city"></i> Bidang
  </div>

  <?= $this->session->flashdata('message'); ?>

  <?= anchor('administrator/bidang/input', '<button class="btn btn-primary mb-3"><i class="fas fa-plus"></i> Tambah Data</button>'); ?>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>Kode Bidang</th>
      <th>Nama Bidang</th>
      <th colspan="2">Aksi</th>
    </tr>

    <?php $i = 1; ?>
    <?php foreach ($bidang as $bd) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $bd->kode_bidang; ?></td>
        <td><?= $bd->bidang; ?></td>
        <td width="20px"><?= anchor('administrator/bidang/update/'. $bd->id_bidang,'<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>' ); ?></td>
        <td width="20px" data-toggle="modal" data-target="#deleteModal"><?= anchor('administrator/bidang/delete/'. $bd->id_bidang,'<div class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></div>' ); ?></td>
      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>
