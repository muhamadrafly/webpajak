<div class="container-fluid">
  <div class="col-lg-10">

  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-info-circle"></i> Update Data
  </div>


<?php foreach ($tentang as $idn) : ?>
  <form action="<?= base_url('administrator/tentang/update_aksi') ?>" method="post">

  <div class="form-group">
      <label for="judul">Judul </label>
      <input type="hidden" name="id" value="<?= $idn->id ?>">
      <input type="text" name="judul" id="judul" class="form-control" placeholder="Judul" value="<?= $idn->judul ?>" >
  </div>
  <div class="form-group">
    <label for="pengertian">Pengertian</label>
    <textarea rows="8" cols="80" type="textarea" name="pengertian" id="pengertian" class="form-control" placeholder="Pengertian"><?= $idn->pengertian ?></textarea>
  </div>
  <div class="form-group">
    <label for="bidang">Penjelasan</label>
    <textarea rows="8" cols="80" type="textarea" name="bidang" id="bidang" class="form-control" placeholder="Bidang"><?= $idn->isi ?></textarea>
  </div>

  <div class="mb-5 pt-2">
    <button type="submit" class="btn btn-primary">Ubah</button>
    <?= anchor('administrator/tentang', '<div class="btn btn-secondary">Kembali</div>') ?>
  </div>
  </form>
<?php endforeach; ?>
  </div>
</div>
