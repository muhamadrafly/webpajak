  <div class="container-fluid">
    <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
      <i class="fas fa-building"></i> Bagian
    </div>

    <?= $this->session->flashdata('message'); ?>

    <?= anchor('administrator/bagian/tambah_bagian', '<button class="btn btn-primary mb-3"><i class="fas fa-plus"></i> Tambah Data</button>'); ?>

    <table class="table table-bordered table-striped table-hover">
      <tr>
        <th>No.</th>
        <th>Kode Bagian</th>
        <th>Nama Bagian</th>
        <th>Bidang</th>
        <th colspan="2">Aksi</th>
      </tr>

      <?php $i = 1; ?>
      <?php foreach ($bagian as $bg): ?>
        <tr>
          <td width="20px"><?= $i ?></td>
          <td><?= $bg->kode_bagian; ?></td>
          <td><?= $bg->bagian; ?></td>
          <td><?= $bg->bidang; ?></td>
          <td width="20px"><?= anchor('administrator/bagian/update/'. $bg->id_bagian,'<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>' ); ?></td>
          <td width="20px"><?= anchor('administrator/bagian/delete/'. $bg->id_bagian,'<div class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></div>' ); ?></td>
        </tr>
        <?php $i++ ?>
      <?php endforeach; ?>
    </table>


</div>
