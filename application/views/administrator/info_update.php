<div class="container-fluid">
  <div class="col-lg-10">

  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-bullhorn"></i> Update Data
  </div>


<?php foreach ($info as $in) : ?>
  <form action="<?= base_url('administrator/info/update_aksi') ?>" method="post">

  <div class="form-group">
    <label for="pertanyaan">Icon</label>
    <input type="hidden" name="id" value="<?= $in->id ?>">
    <input type="text" name="pertanyaan" id="pertanyaan" class="form-control" placeholder="Pertanyaan" value="<?= $in->pertanyaan ?>" >
  </div>
  <div class="form-group">
    <label for="jawaban">Judul </label>
    <input type="text" name="jawaban" id="jawaban" class="form-control" placeholder="Jawaban" value="<?= $in->jawaban ?>" >
  </div>

  <div class="mb-5 pt-2">
    <button type="submit" class="btn btn-primary">Ubah</button>
    <?= anchor('administrator/info', '<div class="btn btn-secondary">Kembali</div>') ?>
  </div>
  </form>
<?php endforeach; ?>
  </div>
</div>
