<div class="container-fluid">
  <div class="col-lg-8">

  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-plus"></i> Tambah Data
  </div>

  <?= form_open_multipart('administrator/karyawan/tambah_karyawan_aksi'); ?>

  <div class="row mb-3">
    <div class="col">
      <label for="nip">NIP</label>
      <input type="text" name="nip" id="nip" class="form-control" value="<?= set_value('nip'); ?>" placeholder="Nip" autofocus>
      <?= form_error('nip', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
    <div class="col">
      <label for="nama">Nama</label>
      <input type="text" name="nama" id="nama" class="form-control" value="<?= set_value('nama'); ?>" placeholder="Nama">
      <?= form_error('nama', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col">
      <label for="alamat">Alamat</label>
      <input type="text" name="alamat" id="alamat" class="form-control" value="<?= set_value('alamat'); ?>" placeholder="Alamat">
      <?= form_error('alamat', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
    <div class="col">
      <label for="email">Email</label>
      <input type="text" name="email" id="email" class="form-control" value="<?= set_value('email'); ?>" placeholder="Email">
      <?= form_error('email', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col">
      <label for="tempat_lahir">Tempat Lahir</label>
      <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" value="<?= set_value('tempat_lahir'); ?>" placeholder="Tempat Lahir">
      <?= form_error('tempat_lahir', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
    <div class="col">
      <label for="tanggal_lahir">Tanggal Lahir</label>
      <input type="date" name="tanggal_lahir" id="tanggal_lahir" value="<?= set_value('tanggal_lahir'); ?>" class="form-control">
      <?= form_error('tanggal_lahir', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
  </div>
  <div class="form-group">
    <label for="jenis_kelamin">Jenis Kelamin</label>
    <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
      <option value="">-- Pilih Jenis Kelamin --</option>
      <option>Laki - Laki</option>
      <option>Perempuan</option>
    </select>
    <?= form_error('jenis_kelamin', '<div class="text-danger small ml-3">', '</div>') ?>
  </div>
  <div class="row mb-3">
    <div class="col">
      <label for="bidang">Bidang</label>
      <select class="form-control" name="bidang" value="<?= set_value('bidang'); ?>" id="bidang">
        <option value="">-- Pilih Bidang --</option>
        <?php foreach ($bidang as $bd) : ?>
        <option value="<?= $bd->bidang ?>"><?= $bd->bidang ?></option>
      <?php endforeach; ?>
      </select>
      <?= form_error('bidang', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
    <div class="col">
      <label for="bagian">Bagian</label>
      <select class="form-control" name="bagian" value="<?= set_value('bagian'); ?>" id="bagian">
        <option value="">-- Pilih Bagian --</option>
        <?php foreach ($bagian as $bd) : ?>
        <option value="<?= $bd->bagian ?>"><?= $bd->bagian ?></option>
      <?php endforeach; ?>
      </select>
      <?= form_error('bagian', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
  </div>
  <div class="form-group">
    <label for="telepon">Nomer Telepon</label>
    <input type="text" name="telepon" id="telepon" value="<?= set_value('telepon'); ?>" class="form-control" placeholder="Nomer Telepon">
    <?= form_error('telepon', '<div class="text-danger small ml-3">', '</div>') ?>
  </div>
  <div class="form-group pt-3">
    <label for="photo">Foto</label>
    <input type="file" name="photo" id="photo" value="value="<?= set_value('photo'); ?>"">
  </div>

  <div class="mb-5 pt-2">
    <button type="submit" class="btn btn-primary">Simpan</button>
    <?= anchor('administrator/karyawan', '<div class="btn btn-secondary">Kembali</div>') ?>
  </div>
  <?php form_close();  ?>
  </div>
</div>
