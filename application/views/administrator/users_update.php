<div class="container-fluid">
  <div class="col-lg-8">

  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-edit"></i> Update Data
  </div>


<?php
  foreach ($user as $user) {
    $id = $user->id;
  }

    $query = "SELECT * FROM user WHERE id=$id";
    $user_asli = $this->db->query($query)->result();

 ?>

<?= form_open_multipart('administrator/users/update_aksi'); ?>
    <?php foreach ($user_asli as $u) : ?>
      <div class="form-group">
          <label for="name">Username</label>
          <input type="hidden" name="id" value="<?= $u->id ?>">
          <input type="text" name="name" id="name" class="form-control" placeholder="Username" value="<?= $u->name ?>" autofocus>
          <?= form_error('name', '<div class="text-danger small ml-1 mt-1">', '</div>') ?>
      </div>
      <div class="form-group">
        <label for="password">password</label>
        <input type="text" name="password" value="<?= $u->password ?>" id="password" class="form-control" placeholder="Password" checked>
          <?= form_error('password', '<small class="text-danger ml-1 mt-1">', '</small>'); ?>
      </div>
      <div class="form-group">
          <label for="email">Email</label>
          <input type="text" name="email" id="email" value="<?= $u->email ?>" class="form-control" placeholder="Email">
          <?= form_error('email', '<div class="text-danger small ml-1 mt-1">', '</div>') ?>
      </div>
      <div class="form-group">
          <label for="level">Level</label>
          <select class="form-control" name="level" id="level">
            <?php
            if ($u->level == 'admin') {
            ?>
            <option value="Admin" selected>Admin</option>
            <option value="Karyawan">Karyawan</option>
          <?php
        } elseif ($u->level == 'karyawan') {
          ?>
          <option value="Admin">Admin</option>
          <option value="Karyawan" selected>Karyawan</option>
        <?php
          } else {
        ?>
          <option value="Admin">Admin</option>
          <option value="Karyawan">Karyawan</option>
        <?php } ?>
          </select>
          <?= form_error('level', '<div class="text-danger small ml-3">', '</div>') ?>
      </div>

      <div class="form-group">
          <label for="blockir">Level</label>
          <select class="form-control" name="blockir" id="blockir">
            <?php
            if ($u->blockir == 'Y') {
            ?>
            <option value="Y" selected>Yes</option>
            <option value="N">No</option>
          <?php
        } elseif ($u->blockir == 'N') {
          ?>
          <option value="Y">Yes</option>
          <option value="N" selected>No</option>
        <?php
          } else {
        ?>
          <option value="Y">Yes</option>
          <option value="N">No</option>
        <?php } ?>
          </select>
          <?= form_error('blockir', '<div class="text-danger small ml-3">', '</div>') ?>
      </div>
      <div class="form-group">
          <input type="hidden" name="image" value="default.jpg">
      </div>

      <div class="mb-5 pt-2">
        <button type="submit" class="btn btn-primary">Simpan</button>
        <?= anchor('administrator/users', '<div class="btn btn-secondary">Kembali</div>') ?>
      </div>
    <?php endforeach; ?>
<?php form_close();  ?>
  </div>
</div>
