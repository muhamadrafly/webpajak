<div class="container-fluid">
  <div class="col-lg-6">

  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-comment-dots"></i> Balas Pesan User
  </div>

    <?php foreach ($kontak as $k ) : ?>
      <form action="<?= base_url('administrator/kontak/kirim_email_aksi') ?>" method="post">

        <div class="form-group">
          <label for="email">Email</label>
          <input type="hidden" name="id_kontak" value="<?= $k->id_kontak ?>">
          <input type="text" name="email" class="form-control" value="<?= $k->email ?>" readonly>
        </div>
        <div class="form-group">
          <label for="subject">Subject</label>
          <input type="text" name="subject" class="form-control">
        </div>
        <div class="form-group">
          <label for="pesan">Pesan</label>
          <textarea name="pesan" class="form-control" rows="8" cols="80" type="text"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Kirim</button>
        <?= anchor('administrator/kontak', '<div class="btn btn-secondary">Kembali</div>') ?>
      </form>
    <?php endforeach; ?>
    </div>
  </div>
