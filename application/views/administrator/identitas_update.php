<div class="container-fluid">
  <div class="col-lg-8">

  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-id-card-alt"></i> Update Data
  </div>


<?php foreach ($identitas as $idn) : ?>
  <form action="<?= base_url('administrator/identitas/update_aksi') ?>" method="post">

  <div class="form-group">
      <label for="nama_website">Judul Website</label>
      <input type="hidden" name="id_identitas" value="<?= $idn->id_identitas ?>">
      <input type="text" name="nama_website" id="nama_website" class="form-control" placeholder="Username" value="<?= $idn->nama_website ?>" >
  </div>
  <div class="form-group">
    <label for="alamat">Alamat</label>
    <input type="text" name="alamat" value="<?= $idn->alamat ?>" id="alamat" class="form-control" placeholder="Alamat">
  </div>
  <div class="form-group">
    <label for="email">Email</label>
    <input type="text" name="email" value="<?= $idn->email ?>" id="email" class="form-control" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="telp">No.Telp</label>
    <input type="text" name="telp" value="<?= $idn->telp ?>" id="telp" class="form-control" placeholder="No.Telp">
  </div>

  <div class="mb-5 pt-2">
    <button type="submit" class="btn btn-primary">Ubah</button>
    <?= anchor('administrator/identitas', '<div class="btn btn-secondary">Kembali</div>') ?>
  </div>
  </form>
<?php endforeach; ?>
  </div>
</div>
