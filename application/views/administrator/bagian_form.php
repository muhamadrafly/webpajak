

<div class="container-fluid">
  <div class="col-lg-5">
    <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
      <i class="fas fa-plus"></i> Form Tambah Data
    </div>

    <form action="<?= base_url('administrator/bagian/tambah_bagian_aksi'); ?>" method="post">

      <div class="form-group">
        <label for="kode_bagian">Kode Bagian</label>
        <input type="text" id="kode_bagian" name="kode_bagian" class="form-control" placeholder="Masukan Kode Bidang" autofocus>
        <?= form_error('kode_bagian', '<div class="text-danger small" ml-3 pt-1>'); ?>
      </div>
      <div class="form-group">
        <label for="bagian">Nama Bagian</label>
        <input type="text" id="bagian" name="bagian" class="form-control" placeholder="Masukan Nama Bidang">
          <?= form_error('bagian', '<div class="text-danger small" ml-3>'); ?>
      </div>
      <div class="form-group">
        <label for="bidang">Nama Bidang</label>
        <select name="bidang" id="bidang" class="form-control">
          <option value="">-- Pilih Bidang --</option>
          <?php foreach ($bidang as $bd ) : ?>
          <option value="<?= $bd->bidang ?>"><?= $bd->bidang ?></option>
          <?php endforeach; ?>
        </select>
          <?= form_error('bagian', '<div class="text-danger small" ml-3>'); ?>
      </div>

      <button type="submit" name="button" class="btn btn-primary">Simpan</button>
      <a class="btn btn-secondary" href="<?= base_url('administrator/bagian'); ?>">Kembali</a>

    </form>
  </div>
</div>
