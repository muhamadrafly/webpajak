<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-newspaper"></i> News
  </div>

  <?= $this->session->flashdata('message'); ?>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>Image</th>
      <th>Judul</th>
      <th>Sumber</th>
      <th>Link</th>
      <th>Tanggal</th>
      <th>Expired</th>
      <th>Isi</th>
      <th>Aksi</th>
    </tr>

    <?php $i = 1; ?>
    <?php foreach ($news as $n) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $n->image; ?></td>
        <td><?= $n->judul; ?></td>
        <td><?= $n->nama_sumber; ?></td>
        <td><?= $n->sumber; ?></td>
        <td><?= $n->tgl; ?></td>
        <td><?= $n->expired; ?></td>
        <td><?= $n->isi; ?></td>
        <td width="20px"><?= anchor('administrator/news/update/' . $n->id, '<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>'); ?></td>
      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>