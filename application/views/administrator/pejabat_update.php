<div class="container-fluid">
  <div class="col-lg-5">
    <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
      <i class="fas fa-user-tie"></i> Form Update
    </div>

    <?php foreach ($pejabat as $pj) : ?>

      <form class="" action="<?= base_url('administrator/pejabat/update_pejabat_aksi'); ?>" method="post">
        <div class="form-group">
          <label for="nama">Nama Bidang</label>
          <input type="hidden" name="id" value="<?= $pj->id ?>">
          <input type="text" name="nama" class="form-control" id="nama" value="<?= $pj->nama ?>" autofocus>
        </div>
        <div class="form-group">
          <label for="nip">Nip</label>
          <input type="text" name="nip" class="form-control" id="nip" value="<?= $pj->nip ?>">
        </div>
        <div class="form-group">
          <label for="jabatan">Jabatan</label>
          <input type="text" name="jabatan" class="form-control" id="jabatan" value="<?= $pj->jabatan ?>" readonly>
        </div>

        <button type="submit" name="button" class="btn btn-primary">Update</button>
        <a class="btn btn-secondary" href="<?= base_url('administrator/pejabat'); ?>">Kembali</a>
      </form>
    <?php endforeach; ?>
  </div>
</div>