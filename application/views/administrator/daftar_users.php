
<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-users-cog"></i> User Control
  </div>

  <?= $this->session->flashdata('message'); ?>

  <?= anchor('administrator/users/tambah_user', '<button class="btn btn-primary mb-3"><i class="fas fa-plus"></i> Tambah Data</button>'); ?>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>Nama</th>
      <th>Email</th>
      <th>Level</th>
      <th>Blockir</th>
      <th colspan="2">Aksi</th>
    </tr>
    <?php

    $query = "SELECT * FROM user";
    $user = $this->db->query($query)->result();

     ?>
    <?php $i = 1; ?>
    <?php foreach ($user as $u) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $u->name; ?></td>
        <td><?= $u->email; ?></td>
        <td width="20px"><?= $u->level; ?></td>
        <td width="20px"><?= $u->blockir; ?></td>
        <td width="20px"><?= anchor('administrator/users/update/'. $u->id,'<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>' ); ?></td>
        <td width="20px"><?= anchor('administrator/users/hapus/'. $u->id,'<div class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></div>' ); ?></td>
      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>
