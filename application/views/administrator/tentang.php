
<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-info-circle"></i> Tentang
  </div>

  <?= $this->session->flashdata('message'); ?>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>Judul</th>
      <th>Pengertian</th>
      <th>Penjelasan</th>
      <th>Aksi</th>
    </tr>

    <?php $i = 1; ?>
    <?php foreach ($tentang as $tg) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $tg->judul; ?></td>
        <td><?= $tg->pengertian; ?></td>
        <td><?= $tg->isi; ?></td>
        <td width="20px"><?= anchor('administrator/tentang/update/'. $tg->id,'<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>' ); ?></td>
      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>
