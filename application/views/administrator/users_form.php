<div class="container-fluid">
  <div class="col-lg-6">

  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-plus"></i> Tambah Data
  </div>

  <?= form_open_multipart('administrator/users/tambah_user_aksi'); ?>

  <div class="form-group">
      <label for="name">Username</label>
      <input type="text" name="name" id="name" class="form-control" placeholder="Username" autofocus>
      <?= form_error('name', '<div class="text-danger small ml-1 mt-1">', '</div>') ?>
  </div>
  <div class="form-group">
    <label for="password">password</label>
    <input type="text" name="password" id="password" class="form-control" placeholder="Password" checked>
    <?php if (form_error('password') == false) : ?>
        <small class="help-block text-danger ml-1 mt-1">( min lenght 6 * digit )</small>
    <?php else : ?>
        <?= form_error('password', '<small class="text-danger ml-1 mt-1">', '</small>'); ?>
    <?php endif; ?>
  </div>
  <div class="form-group">
      <label for="email">Email</label>
      <input type="text" name="email" id="email" class="form-control" placeholder="Email">
      <?= form_error('email', '<div class="text-danger small ml-1 mt-1">', '</div>') ?>
  </div>
  <div class="form-group">
      <label for="level">Level</label>
      <select class="form-control" name="level" id="level">
        <?php
        if ($level == 'admin') {
        ?>
        <option value="Admin" selected>Admin</option>
        <option value="Karyawan">Karyawan</option>
      <?php
        } elseif ($level == 'karyawan') {
      ?>
      <option value="Admin">Admin</option>
      <option value="Karyawan" selected>Karyawan</option>
    <?php
      } else {
    ?>
      <option value="Admin">Admin</option>
      <option value="Karyawan">Karyawan</option>
    <?php } ?>
      </select>
      <?= form_error('level', '<div class="text-danger small ml-3">', '</div>') ?>
  </div>

  <div class="form-group">
      <input type="hidden" name="blockir" value="N">
  </div>
  <div class="form-group">
      <input type="hidden" name="image" value="default.jpg">
  </div>

  <div class="mb-5 pt-2">
    <button type="submit" class="btn btn-primary">Simpan</button>
    <?= anchor('administrator/users', '<div class="btn btn-secondary">Kembali</div>') ?>
  </div>
  <?php form_close();  ?>
  </div>
</div>
