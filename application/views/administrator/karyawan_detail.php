<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-eye"></i> Karyawan Detail
  </div>

  <table class="table table-bordered table-striped table-hover">

    <?php foreach ($detail as $dt) : ?>

      <img class="img-profile rounded-circle mb-3" src="<?= base_url('assets/img/profile/').$dt->photo ?>" style="max-width: 20%">

    <tr>
      <td>NIP</td>
      <td><?= $dt->nip ?></td>
    </tr>
    <tr>
      <td>Nama</td>
      <td><?= $dt->nama ?></td>
    </tr>
    <tr>
      <td>Alamat</td>
      <td><?= $dt->alamat ?></td>
    </tr>
    <tr>
      <td>Email</td>
      <td><?= $dt->email ?></td>
    </tr>
    <tr>
      <td>Tempat Lahir</td>
      <td><?= $dt->tempat_lahir ?></td>
    </tr>
    <tr>
      <td>Tanggal Lahir</td>
      <td><?= $dt->tanggal_lahir  ?></td>
    </tr>
    <tr>
      <td>Jenis Kelamin</td>
      <td><?= $dt->jenis_kelamin ?></td>
    </tr>
    <tr>
      <td>Bidang</td>
      <td><?= $dt->bidang ?></td>
    </tr>
    <tr>
      <td>Bagian</td>
      <td><?= $dt->bagian ?></td>
    </tr>
    <tr>
      <td>Telepon</td>
      <td><?= $dt->telepon ?></td>
    </tr>
  <?php endforeach; ?>

  </table>

  <?= anchor('administrator/karyawan', '<div class="btn btn-secondary mb-5">Kembali</div>') ?>
</div>
