<div class="container-fluid">
  <div class="col-lg-8">

  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-edit"></i> Update Data
  </div>

<?php foreach ($karyawan as $kry) : ?>
  <?= form_open_multipart('administrator/karyawan/update_karyawan_aksi'); ?>

  <div class="row mb-3">
    <div class="col">
      <label for="nip">NIP</label>
      <input type="hidden" name="id" id="id" class="form-control" value="<?= $kry->id ?>">
      <input type="text" name="nip" id="nip" class="form-control" value="<?= $kry->nip ?>" placeholder="Nip" autofocus>
      <?= form_error('nip', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
    <div class="col">
      <label for="nama">Nama</label>
      <input type="text" name="nama" id="nama" class="form-control" value="<?= $kry->nama ?>" placeholder="Nama">
      <?= form_error('nama', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col">
      <label for="alamat">Alamat</label>
      <input type="text" name="alamat" id="alamat" class="form-control" value="<?= $kry->alamat ?>" placeholder="Alamat">
      <?= form_error('alamat', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
    <div class="col">
      <label for="email">Email</label>
      <input type="text" name="email" id="email" class="form-control" value="<?= $kry->email ?>" placeholder="Email">
      <?= form_error('email', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col">
      <label for="tempat_lahir">Tempat Lahir</label>
      <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" value="<?= $kry->tempat_lahir ?>" placeholder="Tempat Lahir">
      <?= form_error('tempat_lahir', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
    <div class="col">
      <label for="tanggal_lahir">Tanggal Lahir</label>
      <input type="date" name="tanggal_lahir" id="tanggal_lahir" value="<?= $kry->tanggal_lahir ?>" class="form-control">
      <?= form_error('tanggal_lahir', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
  </div>
  <div class="form-group">
    <label for="jenis_kelamin">Jenis Kelamin</label>
    <select class="form-control" name="jenis_kelamin" id="jenis_kelamin" value="<?= $kry->jenis_kelamin ?>">
      <option>Laki - Laki</option>
      <option>Perempuan</option>
    </select>
    <?= form_error('jenis_kelamin', '<div class="text-danger small ml-3">', '</div>') ?>
  </div>
  <div class="row mb-3">
    <div class="col">
      <label for="bidang">Bidang</label>
      <select class="form-control" name="bidang" id="bidang" value="<?= $kry->bidang ?>">
        <?php foreach ($bidang as $bd) : ?>
        <option value="<?= $bd->bidang ?>"><?= $bd->bidang ?></option>
      <?php endforeach; ?>
      </select>
      <?= form_error('bidang', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
    <div class="col">
      <label for="bagian">Bagian</label>
      <select class="form-control" name="bagian" value="<?= $kry->bagian ?>" id="bagian">
        <?php foreach ($bagian as $bd) : ?>
        <option value="<?= $bd->bagian ?>"><?= $bd->bagian ?></option>
      <?php endforeach; ?>
      </select>
      <?= form_error('bagian', '<div class="text-danger small ml-3">', '</div>') ?>
    </div>
  </div>
  <div class="form-group">
    <label for="telepon">Nomer Telepon</label>
    <input type="text" name="telepon" id="telepon" value="<?= $kry->telepon ?>" class="form-control" placeholder="Nomer Telepon">
    <?= form_error('telepon', '<div class="text-danger small ml-3">', '</div>') ?>
  </div>
  <div class="form-group pt-3">

    <?php foreach ($detail as $dt) :?>
      <img class="img-profile rounded-circle mb-3" src="<?= base_url('assets/img/profile/').$kry->photo ?>" style="max-width: 20%">
    <?php endforeach; ?>
    <input type="file" name="userfile" id="userfile" value="<?= $kry->photo ?>">
  </div>

  <div class="mb-5 pt-2">
    <button type="submit" class="btn btn-primary">Simpan</button>
    <?= anchor('administrator/karyawan', '<div class="btn btn-secondary">Kembali</div>') ?>
  </div>

  <?php form_close();  ?>

<?php endforeach; ?>
  </div>
</div>
