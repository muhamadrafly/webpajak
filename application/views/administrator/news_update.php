<div class="container-fluid">
  <div class="col-lg-10">

    <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
      <i class="fas fa-newspaper"></i> Update Data
    </div>


    <?php foreach ($news as $n) : ?>
        <?= form_open_multipart('administrator/news/update_aksi'); ?>

        <div class="form-group">
            <input type="hidden" name="id" value="<?= $n->id ?>">
            <img class="rounded-circle d-block mb-3" src="<?= base_url('assets/img/news/').$n->image ?>" style="max-width:20%">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="image" name="image">
            <label class="custom-file-label" for="image">Choose file</label>
          </div>
        </div>
        <div class="form-group">
          <label for="judul">Judul</label>
          <input type="text" name="judul" id="judul" class="form-control" placeholder="Judul" value="<?= $n->judul ?>">
        </div>
        <div class="form-group">
          <label for="nama_sumber">Sumber</label>
          <input type="text" name="nama_sumber" id="nama_sumber" class="form-control" placeholder="nama_sumber" value="<?= $n->nama_sumber ?>">
        </div>
        <div class="form-group">
          <label for="sumber">Link Sumber</label>
          <input type="text" name="sumber" id="sumber" class="form-control" placeholder="Sumber" value="<?= $n->sumber ?>">
        </div>
        <div class="form-group">
          <label for="tgl">Tanggal</label>
          <input type="text" name="tgl" id="tgl" class="form-control" placeholder="Tanggal" value="<?= $n->tgl ?>">
        </div>
        <div class="form-group">
          <label for="expired">Expired</label>
          <input type="text" name="expired" id="expired" class="form-control" placeholder="Expired" value="<?= $n->expired ?>">
        </div>
        <div class="form-group">
          <label for="isi">Isi</label>
          <textarea rows="8" cols="80" type="textarea" name="isi" id="isi" class="form-control" placeholder="Isi"><?= $n->isi ?></textarea>
        </div>

        <div class="mb-5 pt-2">
          <button type="submit" class="btn btn-primary">Ubah</button>
          <?= anchor('administrator/news', '<div class="btn btn-secondary">Kembali</div>') ?>
        </div>
      </form>
    <?php endforeach; ?>
  </div>
</div>
