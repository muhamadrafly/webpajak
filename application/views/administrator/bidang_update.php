<div class="container-fluid">
  <div class="col-lg-5">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-edit"></i> Form Update
  </div>

  <?php foreach ($bidang as $bd) : ?>

    <form class="" action="<?= base_url('administrator/bidang/update_aksi'); ?>" method="post">
      <div class="form-group">
        <label for="kode_bidang">Kode Bidang</label>
        <input type="hidden" name="id_bidang" value="<?= $bd->id_bidang ?>">
        <input type="text" name="kode_bidang" class="form-control" id="kode_bidang" value="<?= $bd->kode_bidang ?>" autofocus>
      </div>
      <div class="form-group">
        <label for="bidang">Nama Bidang</label>
        <input type="text" name="bidang" class="form-control" id="bidang" value="<?= $bd->bidang ?>">
      </div>

      <button type="submit" name="button" class="btn btn-primary">Update</button>
      <a class="btn btn-secondary" href="<?= base_url('administrator/bidang'); ?>">Kembali</a>
    </form>
  <?php endforeach; ?>
  </div>
</div>
