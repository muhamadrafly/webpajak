
<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-bullhorn"></i> Info
  </div>

  <?= $this->session->flashdata('message'); ?>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>Pertanyaan</th>
      <th>Jawaban</th>
      <th>Aksi</th>
    </tr>

    <?php $i = 1; ?>
    <?php foreach ($info as $in) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $in->pertanyaan; ?></td>
        <td><?= $in->jawaban; ?></td>
        <td width="20px"><?= anchor('administrator/info/update/'. $in->id,'<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>' ); ?></td>
      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>
