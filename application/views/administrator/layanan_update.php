<div class="container-fluid">
  <div class="col-lg-10">

  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-file-alt"></i> Update Data
  </div>


<?php foreach ($layanan as $ly) : ?>
  <form action="<?= base_url('administrator/layanan/update_aksi') ?>" method="post">

  <div class="form-group">
    <label for="icon">Icon</label>
    <input type="hidden" name="id" value="<?= $ly->id ?>">
    <input type="text" name="icon" id="icon" class="form-control" placeholder="Icon" value="<?= $ly->icon ?>" >
  </div>
  <div class="form-group">
    <label for="judul">Judul </label>
    <input type="text" name="judul" id="judul" class="form-control" placeholder="Judul" value="<?= $ly->judul ?>" >
  </div>
  <div class="form-group">
    <label for="isi">Isi</label>
    <textarea rows="8" cols="80" type="textarea" name="isi" id="isi" class="form-control" placeholder="Isi"><?= $ly->isi ?></textarea>
  </div>

  <div class="mb-5 pt-2">
    <button type="submit" class="btn btn-primary">Ubah</button>
    <?= anchor('administrator/layanan', '<div class="btn btn-secondary">Kembali</div>') ?>
  </div>
  </form>
<?php endforeach; ?>
  </div>
</div>
