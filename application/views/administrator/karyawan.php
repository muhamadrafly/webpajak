<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-users"></i> Karyawan
  </div>

  <?= $this->session->flashdata('message'); ?>

  <div class="row">
    <div class="col">
      <?= anchor('administrator/karyawan/tambah_karyawan', '<button class="btn btn-primary mb-3"><i class="fas fa-plus"></i> Tambah Data</button>'); ?>
    </div>
    <div class="col-lg-4">
      <input class="form-control" type="text" name="search" id="search" placeholder="Search . . .">
    </div>
  </div>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>NIP</th>
      <th>Nama</th>
      <th>Bidang</th>
      <th>Bagian</th>
      <th colspan="3">Aksi</th>
    </tr>

    <?php $i = 1; ?>
    <?php foreach ($karyawan as $kry) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $kry->nip; ?></td>
        <td><?= $kry->nama; ?></td>
        <td><?= $kry->bidang; ?></td>
        <td><?= $kry->bagian; ?></td>
        <td width="20px"><?= anchor('administrator/karyawan/detail/'. $kry->id,'<div class="btn btn-sm btn-warning"><i class="fas fa-eye"></i></div>' ); ?></td>
        <td width="20px"><?= anchor('administrator/karyawan/update/'. $kry->id,'<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>' ); ?></td>
        <td width="20px"><?= anchor('administrator/karyawan/delete/'. $kry->id,'<div class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></div>' ); ?></td>

      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>
