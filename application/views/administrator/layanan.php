
<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-file-alt"></i> Layanan
  </div>

  <?= $this->session->flashdata('message'); ?>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>Icon</th>
      <th>Judul</th>
      <th>Isi</th>
      <th>Aksi</th>
    </tr>

    <?php $i = 1; ?>
    <?php foreach ($layanan as $ly) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $ly->icon; ?></td>
        <td><?= $ly->judul; ?></td>
        <td><?= $ly->isi; ?></td>
        <td width="20px"><?= anchor('administrator/layanan/update/'. $ly->id,'<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>' ); ?></td>
      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>
