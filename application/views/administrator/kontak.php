
<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-envelope-open-text"></i> Pesan User
  </div>

  <?= $this->session->flashdata('message'); ?>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>Nama</th>
      <th>Email</th>
      <th>No.Telp</th>
      <th>Pesan</th>
      <th colspan="2">Aksi</th>
    </tr>

    <?php $i = 1; ?>
    <?php foreach ($kontak as $kt) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $kt->nama; ?></td>
        <td><?= $kt->email; ?></td>
        <td><?= $kt->telp; ?></td>
        <td><?= $kt->pesan; ?></td>
        <td width="20px"><?= anchor('administrator/kontak/kirim_email/'. $kt->id_kontak,'<div class="btn btn-sm btn-info"><i class="fas fa-comment-dots"></i></div>' ); ?></td>
        <td width="20px"><?= anchor('administrator/kontak/delete/'. $kt->id_kontak,'<div class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></div>'); ?></td>
      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>
