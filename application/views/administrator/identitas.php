
<div class="container-fluid">
  <div class="alert alert-info h5 mb-0 text-gray-800 mb-3" role="alert">
    <i class="fas fa-id-card-alt"></i> Identitas
  </div>

  <?= $this->session->flashdata('message'); ?>

  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>No.</th>
      <th>Judul Website</th>
      <th>Alamat</th>
      <th>Email</th>
      <th>No. Telp</th>
      <th>Aksi</th>
    </tr>

    <?php $i = 1; ?>
    <?php foreach ($identitas as $idn) : ?>
      <tr>
        <td width="20px"><?= $i ?></td>
        <td><?= $idn->nama_website; ?></td>
        <td><?= $idn->alamat; ?></td>
        <td width="20px"><?= $idn->email; ?></td>
        <td width="20px"><?= $idn->telp; ?></td>
        <td width="20px"><?= anchor('administrator/identitas/update/'. $idn->id_identitas,'<div class="btn btn-sm btn-info"><i class="fas fa-edit"></i></div>' ); ?></td>
      </tr>
      <?php $i++ ?>
    <?php endforeach; ?>
  </table>
</div>
