<section id="intro" class="clearfix">
  <div class="container d-flex h-100">
    <div class="row justify-content-center align-self-center">
      <div class="col-md-6 intro-info order-md-first order-last">
        <?php foreach ($identitas as $idn) : ?>
          <h2><?= $idn->nama_website  ?> <span style="font-size:35px">Kabupaten Cianjur</span></h2>
        <?php endforeach; ?>
        <div>
          <a href="#about" class="btn-get-started scrollto"> Telusuri </a>
        </div>
      </div>

      <div class="col-md-5 intro-img order-md-last order-first">
        <img src="<?= base_url('assets/'); ?>img/kantor.png" alt="" class="img-fluid">
      </div>
    </div>

  </div>
</section>

<main id="main">

  <section id="about">

    <div class="container">
      <div class="row">

        <div class="col-lg-4">
          <div class="about-img">
            <img src="<?= base_url('assets/'); ?>img/4.jpg" alt="">
          </div>
        </div>

        <div class="col-lg-8 col-md-6">
          <div class="about-content">
            <?php foreach ($tentang as $tg) : ?>
              <h2 class="wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s"><?= $tg->judul ?></h2>
              <p style="font-size: 18px;" class="wow bounceInUp" data-wow-duration="1.4s"><?= $tg->pengertian ?> </p>
              <ul style="font-size: 17px;" class="box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                <li class="icon"><i class="ion-ios-paper-outline" style="color: #3fcdc7;"></i> Bidang Sekretariat</li>
                <li class="icon"><i class="ion-ios-analytics-outline" style="color: #2282ff;"></i> Bidang Potensi dan Retribusi Pajak Daerah</li>
                <li class="icon"><i class="ion-ios-world-outline" style="color: #2282ff;"></i> Bidang PBB dan BPHTP</li>
                <li class="icon"><i class="ion-ios-bookmarks-outline" style="color: #e98e06;"></i> Bidang Penagihan</li>
              </ul>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>

  </section>

  <section id="services" class="section-bg">
    <div class="container">

      <header class="section-header">
        <h3>Layanan</h3> <br>
      </header>

      <div class="row">
        <?php foreach ($layanan as $ly) : ?>
          <?php if ($ly->id == 1) : ?>
            <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
              <div class="box">
                <div class="icon" style="background: #ecebff;"><i class="<?= $ly->icon ?>" style="color: #8660fe;"></i></div>
                <h4 class="title"><a href=""><?= $ly->judul ?></a></h4>
                <p class="description text-center"><?= $ly->isi ?></p>
              </div>
            </div>
          <?php elseif ($ly->id == 2) : ?>
            <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
              <div class="box">
                <div class="icon" style="background: #fff0da;"><i class="<?= $ly->icon ?>" style="color: #e98e06;"></i></div>
                <h4 class="title"><a href=""><?= $ly->judul ?></a></h4>
                <p class="description text-center"><?= $ly->isi ?></p>
              </div>
            </div>
          <?php else : ?>
            <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="box">
                <div class="icon" style="background: #e6fdfc;"><i class="<?= $ly->icon ?>" style="color: #3fcdc7;"></i></div>
                <h4 class="title"><a href=""><?= $ly->judul ?></a></h4>
                <p class="description text-center"><?= $ly->isi ?></p>
              </div>
            </div>
          <?php endif; ?>
        <?php endforeach; ?>

      </div>

    </div>
  </section>

  <section id="features">
    <div class="container">

      <header class="section-header">
        <h3>Informasi Terbaru</h3> <br>
      </header>



      <div class="row">
        <?php foreach ($news as $n) : ?>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <div class="h-entry">
              <img src="<?= base_url('assets/img/news/') . $n->image ?>" class="img-fluid" width="500px" height="300px">
              <h3><a href="#"><?= $n->judul ?></a></h3>
              <small class="meta mb-4"><a href="<?= $n->sumber ?>"><?= $n->nama_sumber ?></a> <span class="mx-2">&bullet;</span> <?= $n->tgl ?><span class="mx-2">&bullet;</span> <span><?= $n->expired ?></span></small>
              <p><?= $n->isi ?></p>
              <p><a href="<?= $n->sumber ?>" class="btn btn-primary" style="font-size: 15px;">Baca Selengkapnya. . .</a></p>
            </div>
          </div>
        <?php endforeach; ?>
      </div>

    </div>
  </section>


  <section id="call-to-action" class="wow fadeInUp">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 text-center text-lg-left">
          <h3 class="cta-title">Butuh Bantuan?</h3>
          <p class="cta-text"> Hubungi kami untuk panggilan Tindakan dan Pertanyaan, Click button disamping untuk Panggilan Cepat <i class="fa fa-arrow-right"></i></p>
        </div>
        <div class="col-lg-3 cta-btn-container text-center mt-3">
          <nav class="main-nav float-right d-none d-lg-block">
            <a class="cta-btn align-middle " href="#footer">Hubungi Kami</a>
          </nav>

        </div>
      </div>

    </div>
  </section>

  <!-- <section id="clients" class="wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3>Client</h3>
        </header>

        <div class="owl-carousel clients-carousel">
          <img src="<?= base_url('assets/'); ?>img/clients/client-1.png" alt="">
          <img src="<?= base_url('assets/'); ?>img/clients/client-2.png" alt="">
          <img src="<?= base_url('assets/'); ?>img/clients/client-3.png" alt="">
          <img src="<?= base_url('assets/'); ?>img/clients/client-4.png" alt="">
          <img src="<?= base_url('assets/'); ?>img/clients/client-5.png" alt="">
          <img src="<?= base_url('assets/'); ?>img/clients/client-6.png" alt="">
          <img src="<?= base_url('assets/'); ?>img/clients/client-7.png" alt="">
          <img src="<?= base_url('assets/'); ?>img/clients/client-8.png" alt="">
        </div>

      </div>
    </section> -->

  <section id="faq">
    <div class="container">
      <header class="section-header">
        <h3>Pertanyaan yang Sering Ditanyakan</h3>
      </header>

      <ul id="faq-list" class="wow fadeInUp">
        <?php foreach ($info as $in) : ?>
          <li>
            <a data-toggle="collapse" class="collapsed" href="#faq<?= $in->id ?>"><?= $in->pertanyaan ?><i class="ion-android-remove"></i></a>
            <div id="faq<?= $in->id ?>" class="collapse" data-parent="#faq-list">
              <p>
                <?= $in->jawaban ?>
              </p>
            </div>
          </li>
        <?php endforeach; ?>

      </ul>

    </div>
  </section>

</main>
<footer id="footer" class="section-bg">
  <div class="footer-top">
    <div class="container">

      <div class="row">

        <div class="col-lg-6">

          <div class="row">

            <div class="col-sm-7">

              <div class="footer-info">
                <h3>Kontak Kami</h3>
                <?php foreach ($identitas as $idn) : ?>
                  <p><?= $idn->alamat ?></p>
                <?php endforeach; ?>
              </div>

              <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.8108894648926!2d107.1692655143159!3d-6.792850968321259!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e685323eaaaaaab%3A0x2c4a8e4e862f8d59!2sBadan%20Pengelolaan%20Pendapatan%20Daerah%20Kabupaten%20Cianjur!5e0!3m2!1sid!2sid!4v1580133410422!5m2!1sid!2sid" height="255" frameborder="0" style="border:0;" allowfullscreen="" aria-label="BPPD Cianjur"></iframe>

            </div>

            <div class="col-sm-4 pt-5 mt-1">

              <div class="footer-links pt-5 mt-1">
                <p>
                  <?php foreach ($identitas as $idn) : ?>
                    <strong>Phone:</strong> <br><?= $idn->telp ?><br>
                    <strong>Email:</strong> <?= $idn->email ?><br>
                  <?php endforeach; ?>
                </p>
                <h4>Operasinal Time</h4>
                <p>
                  Senin : 08:00 ~ 16:00 <br>
                  Selasa : 08:00 ~ 16:00 <br>
                  Rabu : 08:00 ~ 16:00 <br>
                  Kamis : 08:00 ~ 16:00 <br>
                  Jumat : 08:00 ~ 16:00
                </p>
              </div>


            </div>

          </div>

        </div>

        <div class="col-lg-6">

          <div class="form">

            <h4>Kirim Pesan</h4>
            <?= $this->session->flashdata('message') ?>
            <form action="<?= base_url('layout/kirim_pesan')  ?>" method="post">
              <div class="form-group">
                <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama">
                <?= form_error('nama','<div class="text-danger">') ?>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                  <?= form_error('email','<div class="text-danger">') ?>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="telp" id="telp" placeholder="No Telp">
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" name="pesan" rows="5"  placeholder="Pesan"></textarea>
                    <?= form_error('pesan','<div class="text-danger">') ?>
                    </div>

                    <div class="text-center"><button type="submit">Kirim Pesan</button></div>
                  </form>
                </div>

              </div>



            </div>

          </div>
        </div>
