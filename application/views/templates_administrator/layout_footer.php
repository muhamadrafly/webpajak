



  <div class="container">
    <div class="copyright">
      &copy; Copyright <strong><script>document.write(new Date().getFullYear());</script></strong> Badan Pengelola Pendapatan Daerah | Kabupaten Cianjur
    </div>

  </div>
</footer>

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<script src="<?= base_url('assets/'); ?>lib/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>lib/jquery/jquery-migrate.min.js"></script>
<script src="<?= base_url('assets/'); ?>lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets/'); ?>lib/easing/easing.min.js"></script>
<script src="<?= base_url('assets/'); ?>lib/mobile-nav/mobile-nav.js"></script>
<script src="<?= base_url('assets/'); ?>lib/wow/wow.min.js"></script>
<script src="<?= base_url('assets/'); ?>lib/waypoints/waypoints.min.js"></script>
<script src="<?= base_url('assets/'); ?>lib/counterup/counterup.min.js"></script>
<script src="<?= base_url('assets/'); ?>lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?= base_url('assets/'); ?>lib/isotope/isotope.pkgd.min.js"></script>
<script src="<?= base_url('assets/'); ?>lib/lightbox/js/lightbox.min.js"></script>
<script src="<?= base_url('assets/'); ?>contactform/contactform.js"></script>
<script src="<?= base_url('assets/'); ?>js/main.js"></script>

</body>
</html>
