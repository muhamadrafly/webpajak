<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Badan Pengelola Pendapatan Daerah</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <link href="<?= base_url('assets/'); ?>img/logo.png" rel="icon">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">


  <link href="<?= base_url('assets/'); ?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">


  <link href="<?= base_url('assets/'); ?>lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>lib/lightbox/css/lightbox.min.css" rel="stylesheet">


  <link href="<?= base_url('assets/'); ?>css/style.css" rel="stylesheet">

</head>

<body>

  <header id="header">

    <div id="topbar">
      <div class="container">
        <div class="social-links">
          <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
          <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
          <a href="#" class="whatsapp"><i class="fa fa-whatsapp"></i></a>
          <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
        </div>
      </div>
    </div>

    <div class="container">

      <img src="<?= base_url('assets/'); ?>img/logo.png" class="float-left mr-3" style="max-width:40px">
      <div class="logo float-left">
        <h1 class="text-light"><a href="#intro" class="scrollto"><span>BPPD</span></a></h1>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#intro">Beranda</a></li>
          <li><a href="#about">Profile</a></li>
          <li><a href="#services">Layanan</a></li>
          <li><a href="#features">Berita</a></li>
          <li><a href="#faq">Pertanyaan?</a></li>
          <li><a href="#footer">Kontak</a></li>
          <!-- <li> <a href="<?= base_url('auth') ?>">Login</a> </li> -->
        </ul>
      </nav>

    </div>
  </header>
