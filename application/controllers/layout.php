<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layout extends CI_Controller
{

  public function index()
  {
    $data['title'] = 'BPPD | Kab.Cianjur';

    $data['identitas'] = $this->identitas_model->tampil_data('identitas')->result();
    $data['tentang'] = $this->tentang_model->tampil_data('tentang')->result();
    $data['layanan'] = $this->layanan_model->tampil_data('layanan')->result();
    $data['news'] = $this->news_model->tampil_data('news')->result();
    $data['info'] = $this->info_model->tampil_data('info')->result();
    $data['kontak'] = $this->kontak_model->tampil_data('kontak')->result();
    $this->load->view('templates_administrator/layout_header', $data);
    $this->load->view('layout/index', $data);
    $this->load->view('templates_administrator/layout_footer', $data);
  }

  public function kirim_pesan()
  {

    $this->form_validation->set_rules('nama', 'nama','required|trim',[
      'required' => 'Masukan nama anda terlebih dahulu!'
    ]);
    $this->form_validation->set_rules('email', 'email','required|trim|valid_email',[
      'required' => 'Masukan email anda terlebih dahulu!',
      'valid_email' => 'Email yang anda masukan tidak valid'
    ]);
    $this->form_validation->set_rules('telp', 'telp','trim');
    $this->form_validation->set_rules('pesan', 'pesan','required|trim',[
      'required' => 'Pesan tidak boleh kosong!'
    ]);

    if ($this->form_validation->run() == FALSE) {
      $this->index();
    }else {
      $id = $this->input->post('id_kontak');
      $nama = htmlspecialchars($this->input->post('nama'));
      $email = htmlspecialchars($this->input->post('email'));
      $telp = htmlspecialchars($this->input->post('telp'));
      $pesan = htmlspecialchars($this->input->post('pesan'));

      $data = array(
        'nama'       => $nama,
        'email'        => $email,
        'telp'       => $telp,
        'pesan'        => $pesan
      );

      $this->kontak_model->insert_data($data, 'kontak');
      $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Pesan telah terkirim!</div>');
      redirect('layout/index');
    }
  }

}
