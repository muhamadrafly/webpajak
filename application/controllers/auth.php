<?php

class Auth extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
  }

  public function index()
  {

    $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
      'required' => 'Form Email tidak boleh kosong!',
      'valid_email' => 'Email harus berisi alamat email yang valid'
    ]);
    $this->form_validation->set_rules('password', 'Password', 'required|trim' , [
      'required' => 'Form Password tidak boleh kosong!'
    ]);

    if ($this->form_validation->run() == false) {
      $data['title'] = 'Login Form';

      $this->load->view('templates_administrator/header', $data);
      $this->load->view('auth/login');
      $this->load->view('templates_administrator/footer');

    }else {
      $this->_login();
    }
  }

  private function _login()
  {
      $email = $this->input->post('email');
      $password = $this->input->post('password');

      $user = $this->db->get_where('user', ['email' => $email])->row_array();

      if ($user) {

        if ($user['blockir'] == 'N' ) {

          if (password_verify($password, $user['password'])) {

            $data = [
              'email' => $user['email'],
              'level' => $user['level']
            ];
            $this->session->set_userdata($data);
            if ($user['level'] == 'admin') {
              redirect('administrator/dashboard');
            }else {
              redirect('user');
            }
          }else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Password anda salah!</div>');
            redirect('auth');
        }

      }else {
        $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Account telah diblock!</div>');
        redirect('auth');
      }

    }else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Email belum terdaftar!</div>');
      redirect('auth');
    }

  }

  public function logout()
  {
    $this->session->unset_userdata('email');
    $this->session->unset_userdata('level');

    $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Anda telah log out!</div>');
    redirect('auth');
  }

  public function blocked()
  {
    $data['title'] = 'Akses Ditolak!';

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('auth/blocked');
    $this->load->view('templates_administrator/footer');

  }


}
