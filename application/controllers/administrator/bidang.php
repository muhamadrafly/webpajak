<?php

class Bidang extends CI_Controller{

  public function __construct()
  {
     parent::__construct();

     if (!$this->session->userdata('email')) {
       redirect('auth/blocked');
       }
   }

  public function index()
  {
    $data['title'] = 'Bidang';

    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['bidang'] = $this->bidang_model->tampil_data()->result();
    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/bidang', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function input()
  {
    $data = array(
      'id_bidang' => set_value('id_bidang'),
      'kode_bidang' => set_value('kode_bidang'),
      'bidang' => set_value('bidang')
    );

    $data['title'] = 'Tambah Data Bidang';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/bidang_form', $data);
    $this->load->view('templates_administrator/footer');
  }


  public function input_aksi()
  {

    $this->form_validation->set_rules('kode_bidang', 'kode_bidang','required|trim',[
      'required' => 'Kode Bidang tidak boleh kosong!'
    ]);
    $this->form_validation->set_rules('bidang', 'bidang','required|trim',[
      'required' => 'Nama Bidang tidak boleh kosong!'
    ]);

    if ($this->form_validation->run() == FALSE) {
      $this->input();
    }else {
      $data = array(
        'kode_bidang'     => $this->input->post('kode_bidang', TRUE),
        'bidang'          => $this->input->post('bidang', TRUE)
      );

      $this->bidang_model->input_data($data);
      $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil ditambah!</div>');
      redirect('administrator/bidang');
    }
  }

  public function update($id_bidang)
  {
    $data['title'] = 'Update Data Bidang';
    $where = array('id_bidang' => $id_bidang);
    $data['bidang'] = $this->bidang_model->edit_data($where, 'bidang')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/bidang_update', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update_aksi()
  {
    $id_bidang = $this->input->post('id_bidang');
    $kode_bidang = $this->input->post('kode_bidang');
    $bidang = $this->input->post('bidang');

    $data = array(
      'kode_bidang' => $kode_bidang,
      'bidang' => $bidang
    );

    $where = array(
      'id_bidang' => $id_bidang
    );

    $this->bidang_model->update_data($where,$data,'bidang');
    $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
    redirect('administrator/bidang');
  }

  public function delete($id_bidang)
  {
    $where = array('id_bidang' => $id_bidang);
    $this->bidang_model->hapus_data($where,'bidang');
    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil dihapus!</div>');
    redirect('administrator/bidang');
  }
}
