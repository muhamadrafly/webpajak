<?php

class Kontak extends CI_Controller
{
  public function __construct()
  {
     parent::__construct();

     if (!$this->session->userdata('email')) {
       redirect('auth/blocked');
       }
   }

  public function index()
  {
    $data['title'] = 'Message User';
    $data['kontak'] = $this->kontak_model->tampil_data('kontak')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/kontak', $data);
    $this->load->view('templates_administrator/footer');
  }


  public function delete($id_kontak)
  {
    $where = array('id_kontak' => $id_kontak);
    $this->kontak_model->hapus_data($where,'kontak');
    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Pesan Berhasil dihapus!</div>');
    redirect('administrator/kontak');
  }

  public function kirim_email($id_kontak)
  {
    $data['title'] = 'Balas Pesan User';
    $where = array('id_kontak' => $id_kontak);
    $data['kontak'] = $this->kontak_model->kirim_data($where,'kontak')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/kontak_form', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function kirim_email_aksi()
  {
    $email = $this->input->post('email');
    $subject = $this->input->post('subject');
    $pesan = $this->input->post('pesan');

    $config = [
      'mailtype'    => 'html',
      'charset'     => 'utf-8',
      'protocol'    => 'smtp',
      'smtp_host'   => 'ssl://smtp.gmail.com',
      'smtp_user'   => 'raflyelquraish06@gmail.com',
      'smtp_pass'   => 'rafliiron321',
      'smtp_port'   => 465,
      'crlf'        => "\r\n",
      'newline'     => "\r\n"
    ];

    $this->load->library('email', $config);

    $this->email->from("Badan Pengelola Pendapatan Daerah Kab|Cianjur");

    $this->email->to($email);

    $this->email->subject($subject);

    $this->email->message($pesan);

    if ($this->email->send()) {
      $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Pesan Terkirim!</div>');
      redirect('administrator/kontak');
    }else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Pesan Gagal Terkirim!</div>');
      redirect('administrator/kontak');
    }
  }
}
