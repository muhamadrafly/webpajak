<?php

class Info extends CI_Controller
{
  public function __construct()
  {
     parent::__construct();

     if (!$this->session->userdata('email')) {
       redirect('auth/blocked');
       }
   }

  public function index()
  {
    $data['title'] = 'Info';
    $data['info'] = $this->info_model->tampil_data('info')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/info', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update($id)
  {
    $data['title'] = 'Update Info';
    $where = array('id' => $id);
    $data['info'] = $this->info_model->edit_data($where,'info')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/info_update', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update_aksi()
  {
    $id           = $this->input->post('id');
    $pertanyaan         = $this->input->post('pertanyaan');
    $jawaban        = $this->input->post('jawaban');

    $data = array(
      'pertanyaan'       => $pertanyaan,
      'jawaban'        => $jawaban
    );

    $where = array(
      'id' => $id
    );

    $this->info_model->update_data($where, $data, 'info');
    $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
    redirect('administrator/info');
  }

}
