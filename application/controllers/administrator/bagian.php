<?php

class Bagian extends CI_Controller{

    public function __construct()
    {
       parent::__construct();

       if (!$this->session->userdata('email')) {
         redirect('auth/blocked');
         }
     }
    public function index()
    {
      $data['title'] = 'Bagian';
      $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
      $data['bagian'] = $this->bagian_model->tampil_data('bagian')->result();
      $this->load->view('templates_administrator/header', $data);
      $this->load->view('templates_administrator/sidebar');
      $this->load->view('administrator/bagian', $data);
      $this->load->view('templates_administrator/footer');
    }

    public function tambah_bagian()
    {
      $data['title'] = 'Tambah Data Bagian';
      $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
      $data['bidang'] = $this->bagian_model->tampil_data('bidang')->result();

      $this->load->view('templates_administrator/header', $data);
      $this->load->view('templates_administrator/sidebar');
      $this->load->view('administrator/bagian_form', $data);
      $this->load->view('templates_administrator/footer');
    }

    public function tambah_bagian_aksi()
    {
      $this->form_validation->set_rules('kode_bagian', 'kode_bagian','required',[
        'required' => 'Kode Bagian tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('bagian', 'bagian','required',[
        'required' => 'Nama Bagian tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('bidang', 'bidang','required',[
        'required' => 'Nama Bidang tidak boleh kosong!'
      ]);

      if ($this->form_validation->run() == FALSE) {
        $this-tambah_bagian();
      }else {
        $kode_bagian = $this->input->post('kode_bagian');
        $bagian = $this->input->post('bagian');
        $bidang = $this->input->post('bidang');

        $data = array(
          'kode_bagian' => $kode_bagian,
          'bagian' => $bagian,
          'bidang' => $bidang
        );

        $this->bagian_model->insert_data($data,'bagian');
        $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil ditambah!</div>');
        redirect('administrator/bagian');
      }
    }

    public function update($id_bagian)
    {
      $where = array(
        'id_bagian' => $id_bagian
      );

      $data['title'] = 'Update Data Bagian';
      $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
      $data['bagian'] = $this->db->query("select * from bagian bg, bidang bd where bg.bidang=bd.bidang and bg.id_bagian='$id_bagian'")->result();
      $data['bidang'] = $this->bagian_model->tampil_data('bidang')->result();

      $this->load->view('templates_administrator/header', $data);
      $this->load->view('templates_administrator/sidebar');
      $this->load->view('administrator/bagian_update', $data);
      $this->load->view('templates_administrator/footer');
    }

    public function update_aksi()
    {
      $id_bagian = $this->input->post('id_bagian');
      $kode_bagian = $this->input->post('kode_bagian');
      $bagian = $this->input->post('bagian');
      $bidang = $this->input->post('bidang');

      $data = array(
        'kode_bagian' => $kode_bagian,
        'bagian' => $bagian,
        'bidang' => $bidang
      );

      $where = array(
        'id_bagian' => $id_bagian
      );

      $this->bagian_model->update_data($where,$data,'bagian');
      $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
      redirect('administrator/bagian');
    }

    public function delete($id_bagian)
    {
      $where = array('id_bagian' => $id_bagian);
      $this->bagian_model->hapus_data($where,'bagian');
      $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil dihapus!</div>');
      redirect('administrator/bagian');
    }
}
