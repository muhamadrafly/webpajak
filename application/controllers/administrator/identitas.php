<?php

class Identitas extends CI_Controller
{
  public function __construct()
  {
     parent::__construct();

     if (!$this->session->userdata('email')) {
       redirect('auth/blocked');
       }
   }

  public function index()
  {
    $data['title'] = 'Identitas';
    $data['identitas'] = $this->identitas_model->tampil_data('identitas')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/identitas', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update($id_identitas)
  {
    $data['title'] = 'Update Identitas';
    $where = array('id_identitas' => $id_identitas);
    $data['identitas'] = $this->identitas_model->edit_data($where,'identitas')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/identitas_update', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update_aksi()
  {
    $id_identitas           = $this->input->post('id_identitas');
    $nama_website         = $this->input->post('nama_website');
    $alamat        = $this->input->post('alamat');
    $email     = $this->input->post('email');
    $telp        = $this->input->post('telp');

    $data = array(
      'nama_website'        => $nama_website,
      'alamat'       => $alamat,
      'email'    => $email,
      'telp'       => $telp
    );

    $where = array(
      'id_identitas' => $id_identitas
    );

    $this->identitas_model->update_data($where, $data, 'identitas');
    $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
    redirect('administrator/identitas');
  }

}
