<?php

class Tentang extends CI_Controller
{
  public function __construct()
  {
     parent::__construct();

     if (!$this->session->userdata('email')) {
       redirect('auth/blocked');
       }
   }

  public function index()
  {
    $data['title'] = 'Tentang';
    $data['tentang'] = $this->tentang_model->tampil_data('tentang')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/tentang', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update($id)
  {
    $data['title'] = 'Update Tentang';
    $where = array('id' => $id);
    $data['tentang'] = $this->tentang_model->edit_data($where,'tentang')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/tentang_update', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update_aksi()
  {
    $id           = $this->input->post('id');
    $judul         = $this->input->post('judul');
    $pengertian        = $this->input->post('pengertian');
    $isi        = $this->input->post('isi');

    $data = array(
      'judul'        => $judul,
      'pengertian'       => $pengertian,
      'isi'       => $isi
    );

    $where = array(
      'id' => $id
    );

    $this->tentang_model->update_data($where, $data, 'tentang');
    $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
    redirect('administrator/tentang');
  }

}
