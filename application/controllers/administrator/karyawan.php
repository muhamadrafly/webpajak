<?php

/**
 *
 */
class Karyawan extends CI_Controller
{

  public function __construct()
  {
     parent::__construct();

     if (!$this->session->userdata('email')) {
       redirect('auth/blocked');
     }

   }

   public function index()
   {
     $data['title'] = 'Karyawan';
     $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
     $data['karyawan'] = $this->karyawan_model->tampil_data('karyawan')->result();
     $data['bagian'] = $this->bagian_model->tampil_data('bagian')->result();
     $this->load->view('templates_administrator/header', $data);
     $this->load->view('templates_administrator/sidebar');
     $this->load->view('administrator/karyawan', $data);
     $this->load->view('templates_administrator/footer');
   }

   public function detail($id)
   {
     $data['title'] = 'Detail Karyawan';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
     $data['detail'] = $this->karyawan_model->ambil_id_karyawan($id);
     $this->load->view('templates_administrator/header', $data);
     $this->load->view('templates_administrator/sidebar');
     $this->load->view('administrator/karyawan_detail', $data);
     $this->load->view('templates_administrator/footer');
   }

   public function tambah_karyawan()
   {
     $data['title'] = 'Tambah Data Karyawan';
     $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
     $data['bagian'] = $this->karyawan_model->tampil_data('bagian')->result();
     $data['bidang'] = $this->karyawan_model->tampil_data('bidang')->result();
     $this->load->view('templates_administrator/header', $data);
     $this->load->view('templates_administrator/sidebar');
     $this->load->view('administrator/karyawan_form', $data);
     $this->load->view('templates_administrator/footer');
   }

   public function tambah_karyawan_aksi()
   {
     $this->_rules();

     if ($this->form_validation->run() == FALSE) {
       $this->tambah_karyawan();
     }else {
       $nip                 = $this->input->post('nip');
       $nama                = $this->input->post('nama');
       $alamat              = $this->input->post('alamat');
       $email               = $this->input->post('email');
       $tempat_lahir        = $this->input->post('tempat_lahir');
       $tanggal_lahir       = $this->input->post('tanggal_lahir');
       $jenis_kelamin       = $this->input->post('jenis_kelamin');
       $bidang              = $this->input->post('bidang');
       $bagian              = $this->input->post('bagian');
       $telepon             = $this->input->post('telepon');
       $photo               = $_FILES['photo'];

       if ($photo = '') {

       }else {
          $config['allowed_types'] = 'png|jpg|gif';
          $config['max_size'] = '2048';
          $config['upload_path'] = './assets/img/profile/';

          $this->load->library('upload', $config);
          if (!$this->upload->do_upload('photo')) {
            echo "Gagal Upload"; die();
          }else {
            $photo = $this->upload->data('file_name');
          }
       }

       $data = array(
         'nip'            => htmlspecialchars($nip),
         'nama'           => htmlspecialchars($nama),
         'alamat'         => htmlspecialchars($alamat),
         'email'          => htmlspecialchars($email),
         'tempat_lahir'   => htmlspecialchars($tempat_lahir),
         'tanggal_lahir'  => $tanggal_lahir,
         'jenis_kelamin'  => $jenis_kelamin,
         'bidang'         => $bidang,
         'bagian'         => $bagian,
         'telepon'        => htmlspecialchars($telepon),
         'photo'          => $photo
       );


       $this->karyawan_model->insert_data($data, 'karyawan');
       $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil ditambah!</div>');
       redirect('administrator/karyawan');

     }
   }

   public function update($id)
   {
     $where = array('id' => $id);
     $data['karyawan'] = $this->db->query("select * from karyawan kry, bidang bd where kry.bidang=bd.bidang and kry.id='$id'")->result();

     $data['title'] = 'Update Data Karyawan';
     $data['bidang'] = $this->karyawan_model->tampil_data('bidang')->result();
     $data['bagian'] = $this->karyawan_model->tampil_data('bagian')->result();
     $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
     $data['detail'] = $this->karyawan_model->ambil_id_karyawan($id);

     $this->load->view('templates_administrator/header', $data);
     $this->load->view('templates_administrator/sidebar');
     $this->load->view('administrator/karyawan_update', $data);
     $this->load->view('templates_administrator/footer');
   }

   public function update_karyawan_aksi()
   {
     $this->_rules();

     if ($this->form_validation->run() == FALSE) {
       $this->update();
     }else {
       $id                  = $this->input->post('id');
       $nip                 = $this->input->post('nip');
       $nama                = $this->input->post('nama');
       $email               = $this->input->post('email');
       $alamat              = $this->input->post('alamat');
       $tempat_lahir        = $this->input->post('tempat_lahir');
       $tanggal_lahir       = $this->input->post('tanggal_lahir');
       $jenis_kelamin       = $this->input->post('jenis_kelamin');
       $bidang              = $this->input->post('bidang');
       $bagian              = $this->input->post('bagian');
       $telepon             = $this->input->post('telepon');
       $photo               = $_FILES['userfile']['name'];

       if ($photo) {
          $config['allowed_types'] = 'png|jpg|gif';
          $config['max_size'] = '2048';
          $config['upload_path'] = './assets/img/profile/';

          $this->load->library('upload', $config);
          if ($this->upload->do_upload('userfile')) {
            $userfile = $this->upload->data('file_name');
            $this->db->set('photo', $userfile);
          }else {
            echo "Gagal Upload";
          }
       }

       $data = array(
         'nip'            => $nip,
         'nama'           => $nama,
         'email'          => $email,
         'alamat'         => $alamat,
         'tempat_lahir'   => $tempat_lahir,
         'tanggal_lahir'  => $tanggal_lahir,
         'jenis_kelamin'  => $jenis_kelamin,
         'bidang'         => $bidang,
         'bagian'         => $bagian,
         'telepon'        => $telepon,
       );

       $where = array(
         'id' => $id
       );

       $this->karyawan_model->update_data($where, $data, 'karyawan');
       $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
       redirect('administrator/karyawan');
     }
   }

   public function delete($id)
   {
     $where = array('id' => $id);
     $this->karyawan_model->hapus_data($where,'karyawan');
     $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil dihapus!</div>');
     redirect('administrator/karyawan');
   }

   private function _rules()
   {
     $this->form_validation->set_rules('nip','Nip','required|trim',[
       'required' => 'NIP tidak boleh kosong!'
     ]);
     $this->form_validation->set_rules('nama','Nama','required|trim',[
       'required' => 'Nama tidak boleh kosong!'
     ]);
     $this->form_validation->set_rules('alamat','Alamat','required|trim',[
       'required' => 'Alamat tidak boleh kosong!'
     ]);
     $this->form_validation->set_rules('email','Email','required|trim|valid_email|is_unique[karyawan.email]',[
       'required' => 'Email tidak boleh kosong!',
       'valid_email' => 'Email harus berisi alamat email yang valid',
       'is_unique' => 'Email sudah terdaftar!'
     ]);
     $this->form_validation->set_rules('tempat_lahir','Tempat_lahir','required|trim',[
       'required' => 'Tempat Lahir tidak boleh kosong!'
     ]);
     $this->form_validation->set_rules('tanggal_lahir','Tanggal_lahir','required|trim',[
       'required' => 'Tanggal_lahir tidak boleh kosong!'
     ]);
     $this->form_validation->set_rules('jenis_kelamin','Jenis_kelamin','required|trim',[
       'required' => 'NIP tidak boleh kosong!'
     ]);
     $this->form_validation->set_rules('bidang','Bidang','required|trim',[
       'required' => 'Bidang tidak boleh kosong!'
     ]);
     $this->form_validation->set_rules('bagian','Bagian','required|trim',[
       'required' => 'Bagian tidak boleh kosong!'
     ]);
     $this->form_validation->set_rules('telepon','Telepon');
     $this->form_validation->set_rules('photo','Photo','required|trim',[
       'required' => 'Photo tidak boleh kosong!'
     ]);
   }
}

 ?>
