<?php

/**
 *
 */
class Pejabat extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    if (!$this->session->userdata('email')) {
      redirect('auth/blocked');
    }
  }

  public function index()
  {
    $data['title'] = 'Pejabat';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['pejabat'] = $this->pejabat_model->tampil_data('pejabat')->result();
    $data['bagian'] = $this->bagian_model->tampil_data('bagian')->result();
    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/pejabat', $data);
    $this->load->view('templates_administrator/footer');
  }


  public function update($id)
  {
    $where = array('id' => $id);
    $data['pejabat'] = $this->pejabat_model->edit_data($where, 'pejabat')->result();

    $data['title'] = 'Update Data Karyawan';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/pejabat_update', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update_pejabat_aksi()
  {
    $this->form_validation->set_rules('nama', 'Nama', 'required|trim', [
      'required' => 'Nama tidak boleh kosong!'
    ]);

    if ($this->form_validation->run() == FALSE) {
      $this->update();
    } else {
      $id                  = $this->input->post('id');
      $nama                = $this->input->post('nama');
      $nip                 = $this->input->post('nip');
      $jabatan                 = $this->input->post('jabatan');

      $data = array(
        'nama'           => $nama,
        'nip'           => $nip,
        'jabatan'            => $jabatan
      );

      $where = array(
        'id' => $id
      );

      $this->pejabat_model->update_data($where, $data, 'pejabat');
      $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
      redirect('administrator/pejabat');
    }
  }
}
