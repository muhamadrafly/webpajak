<?php

class News extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();

    if (!$this->session->userdata('email')) {
      redirect('auth/blocked');
    }
  }

  public function index()
  {
    $data['title'] = 'News';
    $data['news'] = $this->news_model->tampil_data('news')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/news', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update($id)
  {
    $data['title'] = 'Update News';
    $where = array('id' => $id);
    $data['news'] = $this->news_model->edit_data($where, 'news')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/news_update', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update_aksi()
  {

      $id             = $this->input->post('id');
      $upload_image   = $_FILES['image']['name'];
      $judul          = $this->input->post('judul');
      $nama_sumber    = $this->input->post('nama_sumber');
      $sumber         = $this->input->post('sumber');
      $tgl            = $this->input->post('tgl');
      $expired        = $this->input->post('expired');
      $isi            = $this->input->post('isi');


      if ($upload_image) {
          $config['allowed_types'] = 'png|jpg|gif';
          $config['max_size'] = '2048';
          $config['upload_path'] = './assets/img/news/';

          $this->load->library('upload', $config);

          if ($this->upload->do_upload('image')) {
            $old_image = $data['news']['image'];
            if ($old_image != 'default.jpg') {
              unlink(FCPATH . 'assets/img/news/' . $old_image);
            }
            $new_image = $this->upload->data('file_name');
            $this->db->set('image', $new_image);
          }else {
            echo $this->upload->display_errors();
          }
      }

    $data = array(
      'image'         => $upload_image,
      'judul'         => $judul,
      'nama_sumber'   => $nama_sumber,
      'sumber'        => $sumber,
      'tgl'           => $tgl,
      'expired'       => $expired,
      'isi'           => $isi
    );

    $where = array(
      'id' => $id
    );

    $this->news_model->update_data($where, $data, 'news');
    $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
    redirect('administrator/news');
  }
}
