<?php

class Users extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();

    if (!$this->session->userdata('email')) {
      redirect('auth/blocked');
    }
  }

  public function index()
  {
    $data['title'] = 'Users Control';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/daftar_users', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function tambah_user()
  {
    $data = array(
      'name'      => set_value('name'),
      'email'     => set_value('email'),
      'password'  => set_value('password'),
      'level'     => set_value('level'),
      'blockir'   => set_value('blockir')
    );

    $data['title'] = 'Tambah User';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/users_form', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function tambah_user_aksi()
  {
    $this->form_validation->set_rules('name', 'Name', 'required|trim', [
      'required' => 'Nama tidak boleh kosong!'
    ]);
    $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]', [
      'required' => 'Password tidak boleh kosong!',
      'min_length' => 'Password telalu pendek!'
    ]);
    $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
      'required' => 'Email tidak boleh kosong!',
      'is_unique' => 'Email sudah terdaftar!'
    ]);
    $this->form_validation->set_rules('level', 'Level', 'required|trim', [
      'required' => 'Level tidak boleh kosong!'
    ]);

    if ($this->form_validation->run() == FALSE) {
      $this->tambah_user();
    } else {
      $data = array(
        'name'                 => htmlspecialchars($this->input->post('name', TRUE)),
        'password'             => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
        'email'                => htmlspecialchars($this->input->post('email', TRUE)),
        'level'                => $this->input->post('level', TRUE),
        'blockir'              => $this->input->post('blockir', TRUE),
        'id_sessions'          => MD5($this->input->post('id_sessions', TRUE)),
        'image'                => $this->input->post('image', TRUE)
      );

      $this->user_model->insert_data($data, 'user');
      $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil ditambah!</div>');
      redirect('administrator/users');
    }
  }

  public function update($id)
  {
    $where = array('id' => $id);

    $data['title'] = 'Update User';
    // $data['user'] = $this->user_model->edit_data($where, 'user')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/users_update', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update_aksi()
  {
    $id           = $this->input->post('id');
    $name         = htmlspecialchars($this->input->post('name'));
    $email        = htmlspecialchars($this->input->post('email'));
    $password     = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
    $level        = $this->input->post('level');
    $blockir      = $this->input->post('blockir');
    $id_sessions  = MD5($this->input->post('id_sessions'));

    $data = array(
      'name'        => $name,
      'email'       => $email,
      'password'    => $password,
      'level'       => $level,
      'blockir'     => $blockir
    );

    $where = array(
      'id' => $id
    );

    $this->user_model->update_data($where, $data, 'user');
    $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
    redirect('administrator/users');
  }

  public function hapus($id)
  {
    $where = array('id' => $id);
    $this->user_model->hapus_data($where, 'user');
    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil dihapus!</div>');
    redirect('administrator/users');
  }
}
