<?php

class Layanan extends CI_Controller
{
  public function __construct()
  {
     parent::__construct();

     if (!$this->session->userdata('email')) {
       redirect('auth/blocked');
       }
   }

  public function index()
  {
    $data['title'] = 'Layanan';
    $data['layanan'] = $this->layanan_model->tampil_data('layanan')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/layanan', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update($id)
  {
    $data['title'] = 'Update Layanan';
    $where = array('id' => $id);
    $data['layanan'] = $this->layanan_model->edit_data($where,'layanan')->result();
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates_administrator/header', $data);
    $this->load->view('templates_administrator/sidebar');
    $this->load->view('administrator/layanan_update', $data);
    $this->load->view('templates_administrator/footer');
  }

  public function update_aksi()
  {
    $id           = $this->input->post('id');
    $icon         = $this->input->post('icon');
    $judul        = $this->input->post('judul');
    $isi        = $this->input->post('isi');

    $data = array(
      'icon'       => $icon,
      'judul'        => $judul,
      'isi'       => $isi
    );

    $where = array(
      'id' => $id
    );

    $this->layanan_model->update_data($where, $data, 'layanan');
    $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Data Berhasil diubah!</div>');
    redirect('administrator/layanan');
  }

}
